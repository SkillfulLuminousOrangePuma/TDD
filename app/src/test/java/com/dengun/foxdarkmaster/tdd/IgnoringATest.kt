package com.dengun.foxdarkmaster.tdd

import org.junit.Ignore
import org.junit.Test
import org.junit.Assert.*
import org.hamcrest.CoreMatchers.*


class IgnoringATest {
    @Ignore("Test is ignored as a demonstration")
    @Test
    fun testSame() {
        assertThat(1, `is`(1))
    }
}

